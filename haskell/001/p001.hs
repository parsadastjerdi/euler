main :: IO()
main  = print $ sum[n | n <- [0..999], (mod n 3) == 0 || (mod n 5) == 0]
